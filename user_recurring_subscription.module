<?php

/**
 * @file
 * Recurring subscription to manage paid membership with recurring facility.
 */
/**
 * Recurring fee states.
 */
define('USER_RECURRING_SUBSCRIPTION_EXPIRED', 0);
define('USER_RECURRING_SUBSCRIPTION_ACTIVE', 1);
define('USER_RECURRING_SUBSCRIPTION_CANCELLED', 2);
define('USER_RECURRING_SUBSCRIPTION_SUSPENDED', 3);
define('USER_RECURRING_SUBSCRIPTION_USD', '$');

/**
 * Implements hook_help().
 */
function user_recurring_subscription_help($path, $arg) {
  switch ($path) {
    case 'admin/help#user_recurring_subscription':
      $output = t("user recurring subscription module is provide a facility");
      $output .= t("&nbsp;to create a recurring profile using credit card.");
      $output .= t("<br />PayFlow Pro payment gateway API's are using in this");
      $output .= t("module to create, update and delete recurring profiles.");
      $output .= t("<br />Create sandbox account !clickhere", array(
        '!clickhere' =>
        l(t('click here'), 'https://ppmts.custhelp.com/app/answers/detail/a_id/929/', array('attributes' => array('target' => '_blank')))));
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function user_recurring_subscription_permission() {
  return array(
    'cancel own subscription profile' => array(
      'title' => t('Cancel own recurring profile'),
    ),
    'edit own credit card details' => array(
      'title' => t('Change own credit card details'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function user_recurring_subscription_menu() {
  $items['admin/config/subscription'] = array(
    'title' => 'User subscription',
    'description' => 'Paid membership management.',
    'position' => 'right',
    'weight' => 0,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
  );

  $items['admin/config/subscription/setting'] = array(
    'title' => 'Payment settings',
    'description' => 'Manage paid user subscription information.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_recurring_subscription_payment_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'user_recurring_subscription.admin.inc',
  );

  $items['admin/config/subscription/manage'] = array(
    'title' => 'Manage subscription',
    'description' => 'Manage subscription plan.',
    'page callback' => 'user_recurring_subscription_manage',
    'access arguments' => array('administer site configuration'),
    'file' => 'user_recurring_subscription.pages.inc',
  );

  $items['admin/config/subscription/add'] = array(
    'title' => 'Add subscription Plan',
    'description' => 'Add new recurring subscription plan.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_recurring_subscription_add_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'user_recurring_subscription.pages.inc',
  );

  $items['admin/config/subscription/%user_recurring_subscription/edit'] = array(
    'title' => 'Add subscription Plan',
    'description' => 'Add new recurring subscription plan.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_recurring_subscription_add_form', 3),
    'access arguments' => array('administer site configuration'),
    'file' => 'user_recurring_subscription.pages.inc',
  );

  $items['admin/config/subscription/%user_recurring_subscription/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_recurring_subscription_delete_confirm', 3),
    'access arguments' => array('administer site configuration'),
    'file' => 'user_recurring_subscription.pages.inc',
  );

  return $items;
}

/**
 * Returns an array of possible currency codes.
 */
function user_recurring_subscription_currency_array() {
  return drupal_map_assoc(array('AUD', 'BRL', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR',
    'GBP', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'MYR', 'NOK', 'NZD', 'PHP',
    'PLN', 'SEK', 'SGD', 'THB', 'TWD', 'USD'));
}

/**
 * Implements hook_load().
 */
function user_recurring_subscription_load($sid) {
  if (!empty($sid)) {
    $object = db_query('SELECT * FROM {subscription} WHERE sid=:sid', array(':sid' => $sid))->fetchObject();

    return $object;
  }
}

/**
 * Implements hook_form_alter().
 */
function user_recurring_subscription_form_user_register_form_alter(&$form, $form_state) {

  $options = user_recurring_subscription_recurring();
  if (!empty($options['options'])) {
    module_load_include('inc', 'user_recurring_subscription', 'user_recurring_subscription.payflow');

    $form['#attached']['css'] = array(
      drupal_get_path('module', 'user_recurring_subscription') . '/css/user_recurring_subscription.css',
    );

    $form['subscription'] = array(
      '#title' => t('Subscription Plan'),
      '#type' => 'radios',
      '#options' => $options['options'],
      '#required' => TRUE,
      '#weight' => -100,
      '#default_value' => !empty($options['default']) ? $options['default'] : '',
    );

    $form += drupal_retrieve_form('user_recurring_subscription_form', $form_state);
    $form['#validate'][] = 'user_recurring_subscription_form_validate';
    array_unshift($form['#submit'], 'user_recurring_subscription_form_submit');
    $form['#submit'][] = 'user_recurring_subscription_info_submit';
  }
}

/**
 * Implements helper function for subscription field on registration form.
 */
function user_recurring_subscription_recurring($sid = NULL) {
  $query = db_select('subscription', 's');
  $query->fields('s', array('sid', 'is_default'));
  $query->condition('s.is_active', 1, '=');
  $query->condition('s.status', 1, '=');
  $result = $query->execute()->fetchAll();

  $options = array();
  if (!empty($result)) {
    foreach ($result as $product) {
      $subscription = user_recurring_subscription_load($product->sid);
      $output = '<span>' . filter_xss($subscription->title) . '</span>';
      $output .= '<span> Setup Fee : ' . USER_RECURRING_SUBSCRIPTION_USD . $subscription->fee . '</span>';
      $output .= '<span>Recurring Fee : ' . USER_RECURRING_SUBSCRIPTION_USD . $subscription->price . '</span>';
      $output .= '<span>Interval : ' . $subscription->regular_interval_unit . '</span>';
      $options['options'][$product->sid] = $output;
      if (!empty($product->is_default)) {
        $options['default'] = $product->sid;
      }
    }
  }
  drupal_alter('user_recurring_subscription_recurring', $options);

  return $options;
}

/**
 * Sends a request to PayPal and returns a response array.
 */
function user_recurring_subscription_api_request($request, $server) {
  // We use $request += to add API credentials so that if a key already exists,
  // it will not be overridden.
  $request += array(
    'USER' => variable_get('user_recurring_subscription_user', ''),
    'PWD' => variable_get('user_recurring_subscription_pass', ''),
    'PARTNER' => variable_get('user_recurring_subscription_partner', ''),
    'VENDOR' => variable_get('user_recurring_subscription_vendor', ''),
    'CUSTIP' => ip_address(),
    'VERBOSITY' => 'MEDIUM',
  );

  $data = drupal_http_build_query($request);

  if (isset($request['ACCT']) && isset($request['AMT'])) {
    $request_id = drupal_get_token($request['ACCT'] . $request['AMT'] . date('YmdGis') . "1");
  }
  else {
    $request_id = drupal_get_token(REQUEST_TIME . date('YmdGis') . "1");
  }

  $headers = array();
  $headers[] = "Content-Type: text/namevalue";
  $headers[] = "X-VPS-Timeout: 30";
  $headers[] = "X-VPS-Request-ID: " . $request_id;
  $post = array(
    'method' => 'POST',
    'data' => $data,
    'timeout' => 30,
    'headers' => $headers,
  );

  $response = array();
  $result = drupal_http_request($server, $post);
  if (isset($result->code) && $result->code == 200) {
    $response = user_recurring_subscription_nvp_to_array($result->data);
  }
  else {
    watchdog('user_recurring_subscription', '!error', array('!error' => '<pre>' . print_r($result, TRUE) . '</pre>'), WATCHDOG_ERROR);
    $response['RESULT'] = FALSE;
  }

  return $response;
}

/**
 * Turns PayFlow's NVP response to an API call into an associative array.
 */
function user_recurring_subscription_nvp_to_array($result) {
  if (empty($result)) {
    return '';
  }

  $response = array();
  $value = explode('&', $result);
  foreach ($value as $token) {
    $key = explode('=', $token);
    $response[$key[0]] = $key[1];
  }

  return $response;
}

/**
 * Helper function to prepare PayFlow Pro request data.
 */
function user_recurring_subscription_request($type = 'R', $action = 'A', $method = 'C') {

  $nvp_request = array(
    'TRXTYPE' => $type,
    'TENDER' => $method,
    'CURRENCY' => variable_get('user_recurring_subscription_currency', 'USD'),
    'ACTION' => $action,
    'TERM' => 0,
    'PROFILENAME' => 'RegularSubscription',
    'COMMENT1' => 'CreateRecurringPaymentsProfile',
  );
  return $nvp_request;
}

/**
 * Implements hook_user_view().
 */
function user_recurring_subscription_user_view($account, $view_mode, $langcode) {
  $profile = user_recurring_subscription_profile($account->uid);
  if (!empty($profile->profile_id)) {
    $profile_data = user_recurring_subscription_payflow_profile($profile->profile_id);
    if (isset($profile_data['RESULT']) && $profile_data['RESULT'] == 0) {
      $data[] = array('data' => 'STATUS :' . $profile_data['STATUS']);
      $data[] = array('data' => 'START :' . $profile_data['START']);
      $data[] = array('data' => 'NEXTPAYMENT :' . $profile_data['NEXTPAYMENT']);
      $data[] = array('data' => 'AMOUNT :' . $profile_data['AMT']);
      $data[] = array('data' => 'ACCT :' . substr($profile_data['ACCT'], -4));
      $data[] = array('data' => 'EXPDATE :' . $profile_data['EXPDATE']);
    }
  }

  if (!empty($data)) {
    $account->content['subscription'] = array(
      '#type' => 'user_profile_item',
      '#title' => t('Subscription'),
      '#markup' => theme('item_list', array('items' => $data)),
      '#attributes' => array('class' => array('')),
    );
  }
}

/**
 * Implements hook_user_delete().
 */
function user_recurring_subscription_user_delete($account) {
  $profile = user_recurring_subscription_profile($account->uid);
  if (!empty($profile->profile_id)) {
    if (user_recurring_subscription_profile_cancel($profile->profile_id)) {
      db_update('recurring')
        ->fields(array('status' => USER_RECURRING_SUBSCRIPTION_CANCELLED))
        ->condition('profile_id', $profile->profile_id)
        ->execute();
    }
  }
}

/**
 * Helper function to get profile information.
 */
function user_recurring_subscription_profile($uid) {
  if (!empty($uid)) {
    $object = db_query('SELECT * FROM {recurring} WHERE uid=:uid', array(':uid' => $uid))->fetchObject();

    return $object;
  }
}

/**
 * Helper function; Get a recurring payments profile from PayPal.
 *
 * @param string $profile_id
 *   The recurring PROFILE ID.
 *
 * @return array
 *   FALSE on failure, otherwise, the NVP response from PayPal.
 */
function user_recurring_subscription_payflow_profile($profile_id) {
  // Build an NVP request.
  $nvp_request = array(
    'TRXTYPE' => 'R',
    'ACTION' => 'I',
    'ORIGPROFILEID' => $profile_id,
  );

  // Post the request, and parse the response.
  $nvp_response = user_recurring_subscription_api_request($nvp_request, variable_get('user_recurring_subscription_server', 'https://pilot-payflowpro.paypal.com'));
  if (isset($nvp_response['STATUS']) && strtoupper($nvp_response['STATUS']) != 'ACTIVE') {
    return FALSE;
  }

  return $nvp_response;
}

/**
 * PayPal website payments pro cancel.
 *
 * Note: the cancel handler gets just one parameter $profile_id. And it is
 * passed by value (not by ref).
 */
function user_recurring_subscription_profile_cancel($profile_id) {
  $nvp_request = array(
    'TRXTYPE' => 'R',
    'ACTION' => 'C',
    'ORIGPROFILEID' => $profile_id,
  );

  // Post the request, and parse the response.
  $nvp_response = user_recurring_subscription_api_request($nvp_request, variable_get('user_recurring_subscription_server', 'https://pilot-payflowpro.paypal.com'));

  if ($nvp_response['RESULT'] == 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_cron().
 *
 * On the renewal time of a recurring fee see if the payment method would
 * like to perform any addition actions.
 *
 * LOCK AGAINST DUPLICATE CALLS OF THIS FUNCTION:
 * ----------------------------------------------
 * A lock is set for a default 30sec - this can be overridden by altering the
 * user_recurring_subscription_cron_timeout variable
 */
function user_recurring_subscription_cron() {
  if (!lock_acquire('user_recurring_subscription_cron', variable_get('user_recurring_subscription_cron_timeout', 30))) {
    // Don't run the recurring payments while another process has this lock.
    return FALSE;
  }
  $fees = user_recurring_subscription_get_fees_for_renew();
  if (!empty($fees)) {
    $fail = $success = $ids = 0;
    foreach ($fees as $fee) {
      if ($reid = user_recurring_subscription_renew($fee)) {
        $success++;
        $ids .= $reid . ',';
      }
      else {
        // Payment attempted but failed.
        $fail++;
      }
      if (lock_may_be_available('user_recurring_subscription_cron')) {
        // Exit if the lock has expired.
        watchdog('lock_may_be_available', serialize($fees));
        break;
      }
    }
    watchdog('user_recurring_subscription_cron', '@success (@ids) recurring fees processed successfully; @fail failed.', array('@success' => $success, '@ids' => $ids, '@fail' => $fail));
  }

  $fees = user_recurring_subscription_fees_for_expiration();
  if (!empty($fees)) {
    foreach ($fees as $fee) {
      user_recurring_subscription_expire($fee);
    }
    watchdog('user_recurring_subscription_expiration', '@count recurring fees expired successfully.', array('@count' => count($fees)));
  }
  lock_release('user_recurring_subscription_cron');
}

/**
 * Get all pending fees that should be renewed.
 */
function user_recurring_subscription_get_fees_for_renew() {
  $fees = array();

  $query = db_select('recurring', 'r')
    ->fields('r', array('reid', 'uid', 'profile_id', 'next_charge', 'attempts'))
    ->condition('r.next_charge', REQUEST_TIME, '<=')
    ->condition('r.status', USER_RECURRING_SUBSCRIPTION_ACTIVE, '=');
  $result = $query->execute()->fetchAll();
  if (!empty($result)) {
    foreach ($result as $fee) {
      $profile = user_recurring_subscription_payflow_profile($fee->profile_id);
      if (!empty($profile['NEXTPAYMENT'])) {
        $year = substr($profile['NEXTPAYMENT'], 4, 4);
        $month = substr($profile['NEXTPAYMENT'], 0, 2);
        $day = substr($profile['NEXTPAYMENT'], 2, 2);
        $time = mktime(0, 0, 0, $month, $day, $year);
        if ($profile['AGGREGATEAMT'] >= $profile['AMT'] &&
          $fee->next_charge <= $time && $profile['STATUS'] == 'ACTIVE') {
          $fee->next_charge = $time;
          $fee->attempts = $fee->attempts + 1;
          $fees[$fee->reid] = $fee;
        }
      }
    }
  }

  return $fees;
}

/**
 * Process a renewal, either from the cron job or manually from a fee handler.
 *
 * @param object $fee
 *   The fee object.
 *
 * @return numeric
 *   The new recurring ID or FALSE if unable to renew fee.
 */
function user_recurring_subscription_renew($fee) {
  $reid = db_update('recurring')
    ->fields(array(
      'next_charge' => $fee->next_charge,
      'attempts' => $fee->attempts,
    ))
    ->condition('reid', $fee->reid, '=')
    ->execute();

  return $reid;
}

/**
 * Get all pending fees that should be expired.
 */
function user_recurring_subscription_fees_for_expiration() {
  $fees = array();
  $time = REQUEST_TIME - variable_get('user_recurring_subscription_expiration_window', 86400);

  $query = db_select('recurring', 'r');
  $query->fields('r', array('reid', 'uid', 'data'));
  $query->condition('r.next_charge', $time, '<=');
  $query->condition('r.status', USER_RECURRING_SUBSCRIPTION_ACTIVE, '=');
  $result = $query->execute()->fetchAll();
  if (!empty($result)) {
    foreach ($result as $fee) {
      $fees[$fee->reid] = $fee;
    }
  }
  return $fees;
}

/**
 * Process a fee expiration.
 *
 * @param object $fee
 *   The recurring fee object.
 */
function user_recurring_subscription_expire($fee) {
  $data = unserialize($fee->data);
  $reid = db_update('recurring')
    ->fields(array('status' => USER_RECURRING_SUBSCRIPTION_EXPIRED))
    ->condition('reid', $fee->reid, '=')
    ->execute();
  $account = user_load($fee->uid);
  $assigned_role = $data['subscription']->assigned_role;
  if (isset($account->roles[$assigned_role])) {
    unset($account->roles[$assigned_role]);
  }
  $remove_role = $data['subscription']->remove_role;
  $account->roles[$remove_role] = TRUE;
  user_save($account);
  watchdog('user_recurring_subscription_expiration_ids', '@ids recurring fees expired successfully.', array('@ids' => $reid));
}
