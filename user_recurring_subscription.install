<?php

/**
 * @file
 * Install, update and uninstall functions for the subscription module.
 */

/**
 * Implements hook_schema().
 */
function user_recurring_subscription_schema() {
  $schema['subscription'] = array(
    'description' => 'The base table for subscription.',
    'fields' => array(
      'sid' => array(
        'description' => 'The primary identifier for a subscription.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The title of this subscription, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'Description of subscription',
        'type' => 'text',
      ),
      'initial_charge_unit' => array(
        'description' => 'The title of this subscription, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'regular_interval_unit' => array(
        'description' => 'The title of this subscription, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'fee' => array(
        'description' => 'The setup fee of the recurring fee.',
        'type' => 'numeric',
        'precision' => 15,
        'scale' => 2,
        'not null' => TRUE,
        'default' => 0.0,
      ),

      'price' => array(
        'description' => 'The amount of the recurring fee.',
        'type' => 'numeric',
        'precision' => 15,
        'scale' => 2,
        'not null' => TRUE,
        'default' => 0.0,
      ),
      'uid' => array(
        'description' => 'The {users}.uid that owns this subscription; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'assigned_role' => array(
        'description' => 'The {users}.uid that owns this subscription; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'remove_role' => array(
        'description' => 'The {users}.uid that owns this subscription; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_default' => array(
        'description' => 'Boolean indicating whether the subscription is published (visible to non-administrators).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_active' => array(
        'description' => 'Boolean indicating whether the subscription is published (visible to non-administrators).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the subscription was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the subscription was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the subscription should be displayed on the front page.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'sid_changed'        => array('changed'),
      'sid_created'        => array('created'),
      'uid'                => array('uid'),
      'status'           => array('status'),
      'is_default'            => array('is_default'),
    ),
    'primary key' => array('sid'),
  );
  
  $schema['recurring'] = array(
    'description' => 'The base table for subscription.',
    'fields' => array(
      'reid' => array(
        'description' => 'The primary identifier for a Recurring profile.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'profile_id' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'fee_handler' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'next_charge' => array(
        'description' => 'The timestamp when the next charge should be performed.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'price' => array(
        'description' => 'The amount of the recurring price.',
        'type' => 'numeric',
        'precision' => 15,
        'scale' => 3,
        'not null' => TRUE,
        'default' => 0.0,
      ),
      'regular_interval' => array(
        'description' => 'The amount of time between charges.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '0',
      ),
      'attempts' => array(
        'description' => 'How many times have we attempted to process this payment.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sid' => array(
        'description' => 'The subscription ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'Serialized array of extra data.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'created' => array(
        'description' => 'Timestamp for when the fee was first attached to the user.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'The status of the recurring fee, e.g., "active" or "expired"',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'profile_id' => array('profile_id'),
      'sid' => array('sid'),
      'uid' => array('uid'),
      'status' => array('status'),
      'next_charge'  => array('next_charge'),
    ),
    'primary key' => array('reid'),
  );
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function user_recurring_subscription_uninstall() {
  variable_del('user_recurring_subscription_server');
  variable_del('user_recurring_subscription_currency');
  variable_del('user_recurring_subscription_user');
  variable_del('user_recurring_subscription_pass');
  variable_del('user_recurring_subscription_vendor');
  variable_del('user_recurring_subscription_partner');
}
